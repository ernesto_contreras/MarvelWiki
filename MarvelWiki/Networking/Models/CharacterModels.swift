//
//  CharacterModel.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 12/5/22.
//

import Foundation

struct MarvelCharacter: Codable {
    var id: Int?
    var name: String?
    var description: String?
    var thumbnail: GeneralImage?
    var comics: CharacterComicList?
    var resourceURI: String?

    func getImage() -> String {
        guard let img = thumbnail else { return "" }
        let url = "\(img.path).\(img.ext)"
        return url
    }

    func getDescription() -> String {
        guard let description = description else {
            return ""
        }
        return description.isEmpty ? LocalizedKeys.Detail.Label.noDescription : description
    }
}

struct CharacterComicList: Codable {
    var items: [GeneralResource]?
}
