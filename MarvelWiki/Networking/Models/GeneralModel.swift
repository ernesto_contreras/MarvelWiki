//
//  GeneralModel.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 20/7/22.
//

import Foundation

struct GeneralResponse<T: Codable>: Codable {
    var code: Int
    var status: String
    var data: GeneralDataResponse<T>
}

struct GeneralDataResponse<T: Codable>: Codable {
    var offset: Int
    var limit: Int
    var total: Int
    var results: [T]
}

struct GeneralResource: Codable {
    var name: String?
    var resource: String?

    enum CodingKeys: String, CodingKey {
        case name
        case resource = "resourceURI"
    }
}

struct GeneralImage: Codable {
    var path: String
    var ext: String
    enum CodingKeys: String, CodingKey {
        case path
        case ext = "extension"
    }
}
