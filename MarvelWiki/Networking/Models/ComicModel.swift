//
//  ComicModel.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 30/5/22.
//

import Foundation

struct MarvelComic: Codable {
    var id: Int?
    var title: String?
    var description: String?
    var thumbnail: GeneralImage?
    var pageCount: Int?
    var prices: [ComicPrice]?
    var resourceURI: String?
    
    func getImage() -> String {
        guard let img = thumbnail else { return "" }
        let url = "\(img.path).\(img.ext)"
        return url
    }
}

struct ComicPrice: Codable {
    var type: String?
    var price: Float?
}
