//
//  ErrorModel.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 13/5/22.
//

import Foundation

struct ErrorModel: Error, Codable {
    var code: String
    var status: String
}
