//
//  DecodeJSON.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 13/5/22.
//

import Foundation

class DecodeJSON {
    static let shared = DecodeJSON()
    let decoder = JSONDecoder()

    /// Decode raw binary Data and converted to an inferred type.
    ///
    /// - Parameter data: raw binary Data
    /// - Returns: inferred type
    public func decode<T: Codable>(data: Data?) -> T? {
        do {
            guard let data = data else { return nil }
            let result = try decoder.decode(T.self, from: data)

            return result

        } catch let DecodingError.dataCorrupted(context) {
            print(context)
            return nil

        } catch let DecodingError.keyNotFound(key, context) {
            print("Key '\(key)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            print("[Decode JSON]  FAILED")
            return nil

        } catch let DecodingError.valueNotFound(value, context) {
            print("Value '\(value)' not found:", context.debugDescription)
            print("codingPath:", context.codingPath)
            print("[Decode JSON]  FAILED")
            return nil

        } catch let DecodingError.typeMismatch(type, context) {
            print("Type '\(type)' mismatch:", context.debugDescription)
            print("codingPath:", context.codingPath)
            print("[Decode JSON]  FAILED")
            return nil

        } catch {
            print("error: ", error)
            print("[Decode JSON]  FAILED")
            return nil
        }
    }
}
