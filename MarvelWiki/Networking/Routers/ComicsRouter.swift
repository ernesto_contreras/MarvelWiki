//
//  ComicsRouter.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 20/7/22.
//

import Foundation
import Alamofire

enum ComicsRouter: NetworkRouter {
    
    case comics(_ id: Int)

    var method: HTTPMethod {
        .get
    }

    var parameters: [String : Any]? {
        switch self {
        case .comics:
            return authParameters()
        }
    }

    var path: String {
        switch self {
        case let .comics(id):
            return "/characters/\(id)/comics"
        }
    }
}
