//
//  CharactersRouter.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 13/5/22.
//

import Foundation
import Alamofire

enum MarvelRouter: NetworkRouter {
    case characters(_ offset: Int = 0)

    var method: HTTPMethod {
        .get
    }
    
    var parameters: [String : Any]? {
        switch self {
        case let .characters(offset):
            var params = authParameters()
            params["offset"] = offset
            return params
        }
    }

    var path: String {
        "/characters"
    }
}
