//
//  APIManager.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 13/5/22.
//

import Foundation
import Alamofire

class ApiClient {
    var currentRequest: DataRequest?
    // MARK: - Functions
    func request(router: NetworkRouter, compleation: @escaping (Result<Data, ErrorModel>) -> Void) {
        currentRequest = AF.request(router)

        currentRequest?.responseData { response in
            self.currentRequest = nil

            switch response.result {
            case .success(let data):

                #if DEBUG
                try? print("URL " + router.asURL().absoluteString)
                print("METHOD " + router.method.rawValue)
                if let parameters = router.parameters, let parametersData = try? JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) {
                    print("PARAMETERS " + String(data: parametersData, encoding: .utf8 )!)
                }
                /// Uncoment if you want to see the json as a dictionary to confirm the returned properties.
//                let responseData = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
//                print("T3ST", responseData)
                #endif

                if let code = response.response?.statusCode, (200...300).contains(code) {
                    compleation(.success(data))
                } else {
                    guard let errorResponse: ErrorModel = DecodeJSON.shared.decode(data: data) else { return }
                    print("T3ST", errorResponse)
                }

            case .failure(let error):
                print("T3ST", error)
            }
        }
    }

    func cancelPreviousRequest() {
        currentRequest?.cancel()
    }

    static func getEnvironmentValue(_ value: Environment) -> String {
        guard let filePath = Bundle.main.path(forResource: "Base", ofType: "plist") else {
            return ""
        }
        let plist = NSDictionary(contentsOfFile: filePath)
        guard let string = plist?.object(forKey: value.rawValue) as? String else {
            fatalError("Couldn't find key \(value.rawValue) in \(filePath).")
        }
        return string
    }
}

enum Environment: String {
    case publicKey
    case privateKey
    case baseUrl
}
