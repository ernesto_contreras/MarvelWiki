//
//  HomeViewController.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 7/5/22.
//

import UIKit

class HomeViewController: MWViewController {

    // MARK: - Properties
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return collectionView
    }()
    private let headerImageView = UIImageView()
    private let searchView = HomeSearchView()
    private let footerView = UIView()
    lazy var presenter: HomePresenterType = {
        let presenter = HomePresenter()
        presenter.viewController = self
        return presenter
    }()
    
    override func viewControllerTheme() -> MWViewController.NavigationStyle {
        .hidden
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    // MARK: - Setup
    private func setup() {
        setupUI()
        presenter.fetchCharacters()
    }
    
    private func setupUI() {
        view.backgroundColor = Asset.Colors.mainRed.color
        addSubviews()
        setupCollectionView()
        setupFooterView()
        setupSearchView()
    }

    private func addSubviews() {
        view.addSubview(collectionView)
        view.addSubview(footerView)
        view.addSubview(searchView)
    }

    private func setupSearchView() {
        searchView.delegate = self
        searchView.anchor(
            to: view, paddingTop: 0, top: view.layoutMarginsGuide.topAnchor,
            paddingLeft: 0, paddingRight: 0, height: 56
        )
    }

    private func setupFooterView() {
        footerView.backgroundColor = Asset.Colors.mainRed.color
        footerView.anchor(
            to: view, paddingTop: 0, top: collectionView.bottomAnchor,
            paddingBottom: 0, bottom: view.layoutMarginsGuide.bottomAnchor,
            paddingLeft: 0, paddingRight: 0, height: 32
        )

        let footerLabel = UILabel()
        footerLabel.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        footerLabel.textColor = Asset.Colors.mainWhite.color
        footerLabel.text = LocalizedKeys.Home.Label.credits
        footerView.addSubview(footerLabel)
        footerLabel.center(inView: footerView)
    }

    private func setupCollectionView() {
        collectionView.backgroundColor = Asset.Colors.mainRed.color
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(
            HomeCollectionViewCell.self,
            forCellWithReuseIdentifier: HomeCollectionViewCell.cellIdentifier
        )
        collectionView.register(
            HomeHeaderView.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
            withReuseIdentifier: HomeHeaderView.identifier
        )
        collectionView.register(
            HomeFooterView.self,
            forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter,
            withReuseIdentifier: HomeFooterView.identifier
        )
        collectionView.anchor(
            to: view, paddingTop: 12, top: searchView.bottomAnchor, paddingBottom: 0,
            bottom: footerView.topAnchor, paddingLeft: 0, paddingRight: 0
        )
    }
    
    // MARK: - Helpers
    
    // MARK: - Actions
}

extension HomeViewController: HomePresenterDelegate {
    func charactersCallSucceeded() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func charactersCallFailed(message: String) {}
}

extension HomeViewController: HomeSearchViewDelegate {
    func searchedText(_ text: String) {
        presenter.searchCharacters(by: text)
        UIView.performWithoutAnimation {
            self.collectionView.reloadSections(IndexSet(integer: 0))
        }
    }
}
