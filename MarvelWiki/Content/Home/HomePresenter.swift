//
//  HomePresenter.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 13/5/22.
//

import Foundation

protocol HomePresenterDelegate: AnyObject {
    func charactersCallSucceeded()
    func charactersCallFailed(message: String)
}

protocol HomePresenterType {
    // Properties
    var viewController: HomePresenterDelegate? { get set }
    var searchedCharacters: [MarvelCharacter] { get set }
    var characters: [MarvelCharacter] { get set }

    // Methods
    func fetchCharacters()
    func searchCharacters(by query: String)
    func getCharacters() -> [MarvelCharacter]
    func fetchMoreCharacters(from lastRow: Int)
}

class HomePresenter: HomePresenterType {

    weak var viewController: HomePresenterDelegate?
    var searchedCharacters: [MarvelCharacter] = []
    var characters: [MarvelCharacter] = []
    var newCharacters: [MarvelCharacter] = [] {
        didSet {
            characters.append(contentsOf: newCharacters)
        }
    }
    private var isSearching: Bool = false
    private var shouldPage: Bool = false
    private var currentOffset = 0

    func fetchCharacters() {
        var router: MarvelRouter = .characters()
        if shouldPage {
            currentOffset += 30
            router = .characters(currentOffset)
        }

        ApiClient().request(router: router) { result in
            switch result {
            case .success(let data):
                guard let response: GeneralResponse<MarvelCharacter> = DecodeJSON.shared.decode(data: data) else {
                    return
                }
                if self.shouldPage {
                    self.newCharacters = response.data.results
                    self.shouldPage = false
                } else {
                    self.characters = response.data.results
                }
                self.viewController?.charactersCallSucceeded()
            case .failure(let error):
                self.viewController?.charactersCallFailed(message: error.localizedDescription)
            }
        }
    }

    func searchCharacters(by query: String) {
        
        let filteredCharacters = characters.filter({
            $0.name?.contains(query) ?? false ||
            $0.comics?.items?.contains(where: {
                $0.name?.contains(query) ?? false
            }) ?? false
        })
        updateSearchStatus(filteredCharacters, status: !query.isEmpty)
    }

    func getCharacters() -> [MarvelCharacter] {
        isSearching ? searchedCharacters : characters
    }

    func fetchMoreCharacters(from lastRow: Int) {
        if !isSearching, lastRow == characters.count - 1 {
            shouldPage = true
            fetchCharacters()
        }
    }

    private func updateSearchStatus(_ searched: [MarvelCharacter], status: Bool) {
        isSearching = status
        searchedCharacters = searched
        NotificationCenter.default.post(name: .hideFooterLoading, object: isSearching)
    }
}
