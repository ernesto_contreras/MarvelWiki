//
//  HomeHeaderView.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 9/5/22.
//

import UIKit

class HomeHeaderView: UICollectionReusableView {
    // MARK: - Constant
    static let identifier = String(describing: HomeHeaderView.self)

    private var imageView = UIImageView()
    private var charactersImageView = UIImageView()

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    // MARK: - Helpers
    private func initialize() {
        backgroundColor = .clear
        addSubview(imageView)
        addSubview(charactersImageView)
        imageView.image = Asset.Assets.logo.image
        imageView.contentMode = .scaleAspectFill
        imageView.anchor(to: self, paddingTop: 0, paddingLeft: 24, paddingRight: -24, height: 160)
        charactersImageView.image = Asset.Assets.characters.image
        charactersImageView.contentMode = .scaleAspectFill
        charactersImageView.anchor(to: self, paddingTop: 0, top: imageView.bottomAnchor, paddingBottom: 0, paddingLeft: 0, paddingRight: 0, height: 80)
    }
}
