//
//  HomeSearchView.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 9/5/22.
//

import UIKit

protocol HomeSearchViewDelegate: AnyObject {
    func searchedText(_ text: String)
}

class HomeSearchView: UIView {
    // MARK: - Properties
    private let containerView = UIView()
    private let imageView = UIImageView()
    weak var delegate: HomeSearchViewDelegate?
    var textField = UITextField()
    
    private var wasTextfieldTapped: Bool = false {
        didSet {
            self.updateSearchBar()
        }
    }

    // MARK: - Init
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - Helpers
    private func initialize() {
        addSubviews()
        setupContainer()
        setupTextView()
        setupSearchIcon()
    }

    private func addSubviews() {
        backgroundColor = Asset.Colors.mainRed.color
        addSubview(containerView)
        containerView.addSubview(textField)
        containerView.addSubview(imageView)
    }

    private func setupContainer() {
        containerView.anchor(to: self, alignVertically: true, alignHorizontally: true, height: 40, width: UIScreen.main.bounds.width - 24)
        containerView.backgroundColor = Asset.Colors.mainWhite.color.withAlphaComponent(0.8)
        containerView.layer.cornerRadius = 20
    }
    
    private func setupTextView() {
        textField.anchor(to: containerView, paddingLeft: 24, paddingRight: 0, right: imageView.leftAnchor, alignVertically: true)
        textField.font = UIFont.systemFont(ofSize: 14, weight: .thin)
        textField.delegate = self
        textField.backgroundColor = .clear
        textField.attributedPlaceholder = NSAttributedString(
            string: LocalizedKeys.Search.placeholder,
            attributes: [.foregroundColor: Asset.Colors.mainRed.color.withAlphaComponent(0.5)])
    }

    private func setupSearchIcon() {
        imageView.image = Asset.Assets.search.image
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = Asset.Colors.mainBlack.color.withAlphaComponent(0.5)
        imageView.anchor(to: containerView, paddingRight: -20, alignVertically: true, height: 24, width: 24)
    }

    private func updateSearchBar() {
        containerView.backgroundColor = wasTextfieldTapped ? Asset.Colors.mainWhite.color.withAlphaComponent(0.9) : Asset.Colors.mainWhite.color.withAlphaComponent(0.8)
        imageView.tintColor = wasTextfieldTapped ? Asset.Colors.mainBlack.color : Asset.Colors.mainBlack.color.withAlphaComponent(0.5)
        textField.textColor = wasTextfieldTapped ? Asset.Colors.mainRed.color : Asset.Colors.mainRed.color.withAlphaComponent(0.5)
    }
}

extension HomeSearchView: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        wasTextfieldTapped = false
        textField.text?.removeAll()
        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
           let textRange = Range(range, in: text),
           let updatedText = text.replacingCharacters(in: textRange,
                                                      with: string) as String? {
            wasTextfieldTapped = updatedText.count != 0
            delegate?.searchedText(updatedText)
        }
        return true
    }
}
