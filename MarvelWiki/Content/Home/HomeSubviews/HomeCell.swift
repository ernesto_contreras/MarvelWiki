//
//  HomeCollectionViewCell.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 8/5/22.
//

import UIKit
import Kingfisher

class HomeCollectionViewCell: UICollectionViewCell {

    // MARK: - Constant
    static let cellIdentifier = String(describing: HomeCollectionViewCell.self)
    
    // MARK: - Properties
    private let imageView = UIImageView()
    private let blurView = UIView()
    private let nameLabel = UILabel()
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        addSubviews()
        setupUI()
    }

    private func setupUI() {
        layer.cornerRadius = 24
        clipsToBounds = true
        setupImageView()
        setupBlurView()
        setupLabel()
    }

    private func addSubviews() {
        addSubview(imageView)
        addSubview(blurView)
        blurView.addSubview(nameLabel)
    }

    private func setupLabel() {
        nameLabel.font = UIFont.systemFont(ofSize: 16, weight: .black)
        nameLabel.textColor = Asset.Colors.mainWhite.color
        nameLabel.center(inView: blurView)
    }

    private func setupImageView() {
        imageView.kf.indicatorType = .activity
        imageView.contentMode = .scaleAspectFill
        imageView.backgroundColor = Asset.Colors.mainWhite.color
        imageView.pinToEdges(ofView: self)
    }

    private func setupBlurView() {
        blurView.backgroundColor = Asset.Colors.mainBlack.color.withAlphaComponent(0.8)
        blurView.anchor(to: self, paddingBottom: 0, paddingLeft: 0, paddingRight: 0, height: 48)
    }
    
    // MARK: - Setup
    func setupCell(name: String?, image: String) {
        nameLabel.text = name

        guard let url = URL(string: image) else {
            return
        }
        
        DispatchQueue.main.async {
            self.imageView.kf.setImage(with: url, placeholder: nil, options: [.transition(.fade(0.3))])
        }
    }
}
