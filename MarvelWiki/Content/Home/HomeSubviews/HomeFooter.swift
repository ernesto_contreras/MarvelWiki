//
//  HomeFooter.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 17/6/22.
//

import UIKit

class HomeFooterView: UICollectionReusableView {

    // MARK: - Constant
    static let identifier = String(describing: HomeFooterView.self)

    var indicator : UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.color = Asset.Colors.mainWhite.color
        view.hidesWhenStopped = true
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup(){
        addObserver()
        addSubview(indicator)
        indicator.anchor(
            to: self, paddingTop: 24, alignHorizontally: true
        )
        indicator.startAnimating()
    }

    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(updateAnimation(_:)), name: .hideFooterLoading, object: nil)
    }

    @objc private func updateAnimation(_ notification: Notification) {
        guard let shouldHide = notification.object as? Bool else { return }
        if shouldHide {
            indicator.stopAnimating()
        } else {
            indicator.startAnimating()
        }
    }
}
