//
//  MWButton.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 30/5/22.
//

import Foundation
import UIKit

class MWButton: UIButton {
    
    enum ButtonType {
        case accept
        case normal
    }
    
    var type: ButtonType = .normal {
        didSet {
          setStyle()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 20
        NSLayoutConstraint.activate([
            heightAnchor.constraint(equalToConstant: 40)
        ])
    }

    private func setStyle() {
        switch type {
        case .accept:
            backgroundColor = Asset.Colors.mainRed.color
            setTitleColor(Asset.Colors.mainWhite.color, for: .normal)
        case .normal:
            backgroundColor = Asset.Colors.mainGray.color
            setTitleColor(Asset.Colors.mainRed.color, for: .normal)
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
