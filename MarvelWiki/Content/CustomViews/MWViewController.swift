//
//  MWViewController.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 24/6/22.
//

import Foundation
import UIKit


class MWViewController: UIViewController {
    enum NavigationStyle {
        case simple
        case hidden
    }

    // MARK: - Properties
    lazy var theme: NavigationStyle = viewControllerTheme()
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupNavigationStyle()
    }
    
    // MARK: - Setup
    private func setupNavigationStyle() {
        setupNavigationBar()
        
        var backImage = UIImage()

        switch theme {
        case .simple:
            navigationController?.setNavigationBarHidden(false, animated: false)
            backImage = Asset.Assets.backArrow.image

            let marvelImage = UIImageView(image: Asset.Assets.logo.image)
            marvelImage.contentMode = .scaleAspectFit

            let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 120, height: 44))
            marvelImage.frame = titleView.bounds
            titleView.addSubview(marvelImage)
            self.navigationItem.titleView = titleView
        case .hidden:
            navigationController?.setNavigationBarHidden(true, animated: false)
        }

        navigationController?.navigationBar.backIndicatorImage = backImage
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)
    }
    
    func viewControllerTheme() -> NavigationStyle {
        .simple
    }
    
    private func setupNavigationBar() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = Asset.Colors.mainRed.color
        navigationController?.navigationBar.standardAppearance = appearance
        navigationController?.navigationBar.scrollEdgeAppearance = appearance
        navigationController?.navigationBar.tintColor = .white
    }
    // MARK: - Actions
}
