//
//  MWAlertViewController.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 30/5/22.
//

import UIKit

struct AlertModel {
    var title: String?
    var message: String?
    var accept: String?
    var cancel: String?
}

class MWAlertViewController: UIViewController {

    lazy var dismissButton: UIButton = {
        let button = UIButton()
        button.setTitle("", for: .normal)
        button.setImage(UIImage(named: "remove"), for: .normal)
        button.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        return button
    }()

    lazy var backgroundButton: UIButton = {
        let button = UIButton()
        button.addTarget(self, action: #selector(dismissButtonTapped), for: .touchUpInside)
        return button
    }()
    
    lazy var containerView: UIView = {
       let view = UIView()
        view.backgroundColor = Asset.Colors.mainRed.color.withAlphaComponent(0.85)
        view.layer.cornerRadius = 24
        return view
    }()

    lazy var alertTitleLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.textColor = .darkGray
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()

    lazy var alertMessageLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        label.textColor = .darkGray.withAlphaComponent(0.7)
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()

    lazy var buttonsStackView: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [alertCancelButton, alertAcceptButton])
        stackView.axis = .horizontal
        stackView.spacing = 12
        return stackView
    }()
    
    lazy var alertAcceptButton: MWButton = {
       let button = MWButton()
        button.type = .accept
        button.widthAnchor.constraint(
            equalToConstant: UIScreen.main.bounds.width / 2.5
        ).isActive = true
        return button
    }()

    lazy var alertCancelButton: MWButton = {
       let button = MWButton()
        button.type = .normal
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black.withAlphaComponent(0.8)
        addSubviews()
        alignSubviews()
        
    }

    func setupAlert(_ alert: AlertModel) {
        alertTitleLabel.text = alert.title
        alertMessageLabel.text = alert.message
        alertAcceptButton.setTitle(alert.accept, for: .normal)
        alertCancelButton.setTitle(alert.cancel, for: .normal)
    }

    private func addSubviews() {
        view.addSubview(backgroundButton)
        view.addSubview(containerView)
        containerView.addSubview(alertTitleLabel)
        containerView.addSubview(dismissButton)
        containerView.addSubview(alertMessageLabel)
        containerView.addSubview(buttonsStackView)
    }

    private func alignSubviews() {
        backgroundButton.pinToEdges(ofView: self.view)
        containerView.anchor(to: self.view, alignVertically: true, alignHorizontally: true, width: UIScreen.main.bounds.width - 32)
        dismissButton.anchor(to: containerView, paddingTop: 12, paddingRight: -12, height: 32, width: 32)
        alertTitleLabel.anchor(to: containerView, paddingTop: 24, paddingBottom: -16, bottom: alertMessageLabel.topAnchor, paddingLeft: 24, paddingRight: -24, height: 24)
        alertMessageLabel.anchor(to: containerView, paddingBottom: -24, bottom: buttonsStackView.topAnchor, paddingLeft: 24, paddingRight: -24)
        buttonsStackView.anchor(to: containerView, paddingBottom: -24, paddingLeft: 24, paddingRight: -24)
    }
    
    @objc func dismissButtonTapped() {
        self.dismiss(animated: true)
    }
}
