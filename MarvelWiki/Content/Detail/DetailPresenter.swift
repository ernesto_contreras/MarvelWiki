//
//  DetailViewModel.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 24/6/22.
//

import Foundation

protocol DetailPresenterDelegate: AnyObject {
    func comicsCallSucceeded()
    func comicsCallFailed(_ message: String)
}

protocol DetailPresenterType {
    // Properties
    var viewController: DetailPresenterDelegate? { get set }
    var character: MarvelCharacter? { get set }
    var comics: [MarvelComic]? { get set }

    // Methods
    func fetchComics()
    func getComics() -> [MarvelComic]
}

class DetailPresenter: DetailPresenterType {
    var viewController: DetailPresenterDelegate?
    var character: MarvelCharacter?
    var comics: [MarvelComic]?
    
    func fetchComics() {
        let router: ComicsRouter = .comics(getID())

        ApiClient().request(router: router) { result in
            switch result {
            case .success(let data):
                guard let response: GeneralResponse<MarvelComic> = DecodeJSON.shared.decode(data: data) else {
                    return
                }
                self.comics = response.data.results
                self.viewController?.comicsCallSucceeded()
            case .failure(let error):
                self.viewController?.comicsCallFailed(error.localizedDescription)
            }
        }
    }

    func getComics() -> [MarvelComic] {
        guard let comics = comics else {
            return []
        }
        return comics
    }

    private func getID() -> Int {
        guard let id = character?.id else { return 0 }
        return id
    }
}
