//
//  DetailViewController.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 22/6/22.
//

import Kingfisher
import UIKit

class DetailViewController: MWViewController {
    // MARK: - UI Properties
    lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        return scrollView
    }()

    lazy var contentView: UIView = {
        let view = UIView()
        view.backgroundColor = Asset.Colors.mainRed.color
        return view
    }()
    
    lazy var imageView: UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()

    lazy var labelContainerView: UIView = {
        let view = UIView()
        view.backgroundColor = Asset.Colors.mainBlack.color.withAlphaComponent(0.8)
        return view
    }()

    lazy var nameLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 32, weight: .bold)
        label.textColor = Asset.Colors.mainWhite.color
        return label
    }()

    lazy var descriptionTitleLabel: UILabel = {
        let label = UILabel()
        label.text = LocalizedKeys.Detail.Label.description.uppercased()
        label.font = UIFont.systemFont(ofSize: 28, weight: .bold)
        label.textColor = Asset.Colors.mainBlack.color
        return label
    }()

    lazy var descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14, weight: .light)
        label.textColor = Asset.Colors.mainBlack.color
        label.numberOfLines = 0
        return label
    }()

    lazy var descriptionStackView: UIStackView = {
        let stackView = UIStackView(
            arrangedSubviews: [descriptionTitleLabel, descriptionLabel]
        )
        stackView.axis = .vertical
        stackView.distribution = .fill
        stackView.spacing = 12
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()

    lazy var comicCollectionView: ComicCollectionView = {
        return ComicCollectionView(frame: .zero)
    }()

    // MARK: - Properties
    lazy var presenter: DetailPresenterType = {
        let presenter = DetailPresenter()
        presenter.viewController = self
        return presenter
    }()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setupConstraints()
    }
    
    // MARK: - Setup
    private func setup() {
        view.backgroundColor = Asset.Colors.mainRed.color
        addSubviews()
        populateUI()
        presenter.fetchComics()
    }

    private func addSubviews() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(imageView)
        contentView.addSubview(labelContainerView)
        labelContainerView.addSubview(nameLabel)
        contentView.addSubview(descriptionStackView)
        contentView.addSubview(comicCollectionView)
    }

    private func setupConstraints() {
        scrollView.pinToEdges(ofView: view)
        
        contentView.pinToEdges(ofView: scrollView)
        contentView.setSize(width: view.widthAnchor)

        imageView.anchor(to: contentView, paddingTop: 0, paddingLeft: 0, paddingRight: 0, height: UIScreen.main.bounds.height / 3)

        labelContainerView.anchor(to: imageView, paddingBottom: 0, paddingLeft: 0, paddingRight: 0, height: 80)
        nameLabel.anchor(to: labelContainerView, paddingLeft: 24, paddingRight: -24, alignVertically: true)

        descriptionStackView.anchor(
            to: contentView, paddingTop: 16, top: imageView.bottomAnchor, paddingLeft: 24, paddingRight: -24
        )

        comicCollectionView.anchor(to: contentView, paddingTop: 16, top: descriptionStackView.bottomAnchor, paddingBottom: -24, paddingLeft: 0, paddingRight: 0, height: UIScreen.main.bounds.width / 1.5)
    }
    
    private func populateUI() {
        
        guard let image = presenter.character?.getImage(), let url = URL(string: image) else {
            return
        }
        
        DispatchQueue.main.async {
            self.imageView.kf.setImage(with: url, placeholder: nil, options: [.transition(.fade(0.3))])
        }

        nameLabel.text = presenter.character?.name?.uppercased()
        descriptionLabel.text = presenter.character?.getDescription()
    }
}

extension DetailViewController: DetailPresenterDelegate {
    func comicsCallSucceeded() {
        comicCollectionView.setComics(presenter.getComics())
    }
    
    func comicsCallFailed(_ message: String) {}
}
