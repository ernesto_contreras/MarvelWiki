//
//  ComicCollectionView.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 26/6/22.
//

import Foundation
import UIKit

class ComicCollectionView: UIView {
    lazy var collectionView: UICollectionView = {
        let collectionLayout = UICollectionViewFlowLayout()
        collectionLayout.scrollDirection = .horizontal
        let collectionView = UICollectionView(
            frame: .zero, collectionViewLayout: collectionLayout
        )
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear

        collectionView.register(
            ComicCollectionCell.self,
            forCellWithReuseIdentifier: ComicCollectionCell.cellIdentifier
        )
        return collectionView
    }()

    private var indicator : UIActivityIndicatorView = {
        let view = UIActivityIndicatorView()
        view.color = Asset.Colors.mainWhite.color
        view.hidesWhenStopped = true
        return view
    }()

    private var comics: [MarvelComic]?

    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
        fatalError("init(coder:) has not been implemented")
    }

    func setComics(_ comics: [MarvelComic]) {
        self.comics = comics
        collectionView.reloadData()
        indicator.stopAnimating()
    }

    private func setup() {
        self.backgroundColor = .clear
        self.addSubview(collectionView)
        collectionView.pinToEdges(ofView: self)
        setupIndicator()
    }
    
    private func setupIndicator() {
        addSubview(indicator)
        indicator.anchor(
            to: self, alignVertically: true, alignHorizontally: true
        )
        indicator.startAnimating()
    }
}

extension ComicCollectionView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        comics?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ComicCollectionCell.cellIdentifier, for: indexPath)
                as? ComicCollectionCell else {
                    return UICollectionViewCell()
                }
        if let comic = self.comics?[indexPath.item] {
            cell.setupCell(comic)
        }
        return cell
    }
}

extension ComicCollectionView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = UIScreen.main.bounds.width / 1.8
        return CGSize(width: size - 32, height: size)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
    }
}
