//
//  ComicCell.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 26/6/22.
//

import UIKit

class ComicCollectionCell: UICollectionViewCell {
    // MARK: - Constant
    static let cellIdentifier = String(describing: ComicCollectionCell.self)
    
    // MARK: - Properties
    lazy var imageView: UIImageView = {
       let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .black
        imageView.kf.indicatorType = .activity
        return imageView
    }()

    lazy var nameLabel: UILabel = {
       let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 13, weight: .semibold)
        label.numberOfLines = 0
        label.textAlignment = .center
        label.textColor = Asset.Colors.mainWhite.color
        return label
    }()
    
    // MARK: - Lifecycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        imageView.layer.cornerRadius = imageView.frame.width / 2
    }
    
    // MARK: - Setup
    private func setup() {
        addSubviews()
        setupConstraints()
    }

    private func addSubviews() {
        addSubview(imageView)
        addSubview(nameLabel)
    }
    
    private func setupConstraints() {
        let size = UIScreen.main.bounds.width / 2.4
        imageView.anchor(
            to: self, paddingTop: 0, alignHorizontally: true, height: size, width: size
        )

        nameLabel.anchor(
            to: self, paddingTop: 8, top: imageView.bottomAnchor,
            paddingBottom: 0, alignHorizontally: true, width: size + 12
        )
    }

    func setupCell(_ comic: MarvelComic) {
        guard let url = URL(string: comic.getImage()) else {
            return
        }

        DispatchQueue.main.async {
            self.imageView.kf.setImage(
                with: url, placeholder: Asset.Assets.notFound.image,
                options: [.transition(.fade(0.3))]
            )
        }

        nameLabel.text = comic.title
    }
    
    // MARK: - Actions
}
