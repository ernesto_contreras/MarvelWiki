//
//  String.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 13/5/22.
//

import CryptoKit

extension String {
    func md5Hash() -> String {
        return Insecure.MD5.hash(data: self.data(using: .utf8)!).map { String(format: "%02hhx", $0) }.joined()
    }
}
