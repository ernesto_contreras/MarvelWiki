//
//  NotificationName.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 17/6/22.
//

import Foundation

extension Notification.Name {
    static let hideFooterLoading = Notification.Name("hideLoading")
}
