//
//  UIView.swift
//  MarvelWiki
//
//  Created by Ernesto Jose Contreras Lopez on 7/5/22.
//

import UIKit

//  MARK: - Quick layout
extension UIView {
    
    func setSize(height: NSLayoutDimension? = nil, width: NSLayoutDimension? = nil) {
        self.translatesAutoresizingMaskIntoConstraints = false
        if let height = height {
            self.heightAnchor.constraint(equalTo: height).isActive = true
        }

        if let width = width {
            self.widthAnchor.constraint(equalTo: width).isActive = true
        }
    }
    
    func center(inView view: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        self.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        self.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    func pinToEdges(ofView view: UIView, atSafeArea: Bool = false) {
        self.translatesAutoresizingMaskIntoConstraints = false

        if atSafeArea {
            NSLayoutConstraint.activate([
                self.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
                self.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                self.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor),
                self.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor)
            ])
        } else {
            NSLayoutConstraint.activate([
                self.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                self.topAnchor.constraint(equalTo: view.topAnchor),
                self.leftAnchor.constraint(equalTo: view.leftAnchor),
                self.rightAnchor.constraint(equalTo: view.rightAnchor)
            ])
        }
    }
    
    func anchor(to view: UIView,
                paddingTop: CGFloat? = nil, top: NSLayoutYAxisAnchor? = nil,
                paddingBottom: CGFloat? = nil, bottom: NSLayoutYAxisAnchor? = nil,
                paddingLeft: CGFloat? = nil, left: NSLayoutXAxisAnchor? = nil,
                paddingRight: CGFloat? = nil, right: NSLayoutXAxisAnchor? = nil,
                alignVertically: Bool = false, alignHorizontally: Bool = false,
                height: CGFloat? = nil, width: CGFloat? = nil) {
        translatesAutoresizingMaskIntoConstraints = false

        if let paddingTop = paddingTop {
            if let top = top {
                topAnchor.constraint(equalTo: top, constant: paddingTop).isActive = true
            } else {
                topAnchor.constraint(equalTo: view.topAnchor, constant: paddingTop).isActive = true
            }
        }

        if let paddingBottom = paddingBottom {
            if let bottom = bottom {
                bottomAnchor.constraint(equalTo: bottom, constant: paddingBottom).isActive = true
            } else {
                bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: paddingBottom).isActive = true
            }
        }
        
        if let paddingLeft = paddingLeft {
            if let left = left {
                leftAnchor.constraint(equalTo: left, constant: paddingLeft).isActive = true
            } else {
                leftAnchor.constraint(equalTo: view.leftAnchor, constant: paddingLeft).isActive = true
            }
        }
        
        if let paddingRight = paddingRight {
            if let right = right {
                rightAnchor.constraint(equalTo: right, constant: paddingRight).isActive = true
            } else {
                rightAnchor.constraint(equalTo: view.rightAnchor, constant: paddingRight).isActive = true
            }
        }

        if let height = height {
            heightAnchor.constraint(equalToConstant: height).isActive = true
        }
        
        if let width = width {
            widthAnchor.constraint(equalToConstant: width).isActive = true
        }

        if alignVertically {
            centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        }

        if alignHorizontally {
            centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        }
    }
}
