// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum LocalizedKeys {

  internal enum Alert {
    /// Here we are, having an alert test because well, shit happens get the whisky my friend this is a test obviously this is a test a test for a test test etst
    internal static let message = LocalizedKeys.tr("Localizable", "Alert.message")
    /// This is an alert test
    internal static let title = LocalizedKeys.tr("Localizable", "Alert.title")
    internal enum Button {
      /// Accept
      internal static let accept = LocalizedKeys.tr("Localizable", "Alert.button.accept")
      /// Cancel
      internal static let cancel = LocalizedKeys.tr("Localizable", "Alert.button.cancel")
    }
  }

  internal enum Detail {
    internal enum Label {
      /// Description
      internal static let description = LocalizedKeys.tr("Localizable", "Detail.label.description")
      /// There is no description available for this character
      internal static let noDescription = LocalizedKeys.tr("Localizable", "Detail.label.noDescription")
    }
  }

  internal enum Home {
    internal enum Label {
      /// Data provided by Marvel. © 2014 Marvel
      internal static let credits = LocalizedKeys.tr("Localizable", "Home.label.credits")
    }
  }

  internal enum Search {
    /// Search your favorite Character by name or comic
    internal static let placeholder = LocalizedKeys.tr("Localizable", "Search.placeholder")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension LocalizedKeys {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
